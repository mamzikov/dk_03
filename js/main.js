requirejs.config({
	baseUrl: 'js',
	paths: {
		'jquery': 'vendor/jquery-3.3.1.min',
		'bootstrap': 'vendor/bootstrap/js/bootstrap.bundle.min',
		'TimelineMax': 'vendor/TimelineMax.min',
		'TweenMax': 'vendor/TweenMax.min',
		'TweenLite': 'vendor/TweenLite.min',
		'fullpage': 'vendor/jquery.fullpage.min',
		'photoswipe': 'vendor/photoSwipe/photoswipe.min',
		'photoswipe-ui': 'vendor/photoSwipe/photoswipe-ui-default.min',
		'photogallery': 'vendor/init-photogallery',
	},
	waitSeconds: 0
});

require([
	'jquery',
	'TweenMax',
	'TweenLite',
	'TimelineMax',
	'fullpage',
	'bootstrap',
	'photogallery',
], function ($, TweenMax, TweenLite, fullpage) {

	//Открытие и закрытие меню и страниц
	function actionBlock (el, block) {
		var overlay = '.js-overlay';
		var closeButton = $(block).find('.js-close');

		$(el).on('click', function() {
			var level = $(this).attr('data-level') || 1;

			setTimeout(function() {
				$(overlay + '[data-level="' + level + '"]').toggleClass('active');
			}, 300);

			$(block + '[data-level="' + level + '"]').toggleClass('opened');
		});

		$(overlay).on('click', function() {
			var level = $(this).attr('data-level');

			$(this).removeClass('active');
			$(block + '[data-level="' + level + '"]').removeClass('opened');
		});

		closeButton.on('click', function() {
			var level = $(this).attr('data-level');

			$(overlay + '[data-level="' + level + '"]').removeClass('active');
			$(block + '[data-level="' + level + '"]').removeClass('opened');
		});
	}

	actionBlock('.js-menu-action', '.js-menu');

	actionBlock('.js-page-action', '.js-page');

	//временная функция показа нужного контента
	$('.js-page-action').on('click', function() {
		var pages = $('.Page__content');
		var button = $(this);

		pages.each(function() {
			if($(this).attr('data-page') == button.attr('data-page') ) {
				$(this).addClass('active');
				if ($(this).find('js-counter-block')) {
					counterAnimate(3162000, 3163264, '.js-counter-cup');
					counterAnimate(0, 300, '.js-counter-clients');
					counterAnimate(0, 5, '.js-counter-years');
					counterAnimate(0, 15, '.js-counter-profit');
				}
			} else {
				if(+button.attr('data-level') == 1) {
					$(this).removeClass('active');
				}
			}
		});
	});


	//Preloader
	var preLodr = $('#preloader');
	if (preLodr) {
		$('#preloader').fadeOut();
		$('.loading').delay(350).fadeOut('slow');
		$('body').delay(350).css({ 'overflow': 'visible' });
	}

	// Модуль анимирования объектов страницы
	var animationBlock = (function () {

		function animationFadeInUp(elem) {
			var item = elem.find('.js-animation-fadeInUp');

			TweenLite.to(item, 0.6, { y: -30, opacity: 1, ease: Power2.easeOut });
		}

		function reverseAnimationFadeInUp(elem) {
			var item = elem.find('.js-animation-fadeInUp');

			TweenLite.to(item, 0.6, { y: 30, opacity: 0 });
		}

		function animationFadeInDown(elem) {
			var item = elem.find('.js-animation-fadeInDown');

			TweenLite.to(item, 0.6, { y: 30, opacity: 1, ease: Power2.easeOut });

		}

		function reverseAnimationFadeInDown(elem) {
			var item = elem.find('.js-animation-fadeInDown');

			TweenLite.to(item, 0.6, { y: -30, opacity: 0 });
		}

		function animationFadeInUpMulti(elem, step) {
			var items = elem.find('.js-animation-fadeInUp');
			var delayAnimation = step;

			for (var i = 0; i < items.length; i++) {
				TweenLite.to(items[i], 0.6, { y: -30, opacity: 1, ease: Power2.easeOut, delay: delayAnimation });
				delayAnimation = delayAnimation + step;
			}
		}

		function reverseAnimationFadeInUpMulti(elem) {
			var items = elem.find('.js-animation-fadeInUp');

			for (var i = 0; i < items.length; i++) {
				TweenLite.to(items[i], 0.6, { y: 30, opacity: 0 });
			}
		}

		function animationFadeIn(elem) {
			var item = elem.find('.js-animation-fadeIn');

			TweenLite.to(item, 0.6, { opacity: 1 });
		}

		function reverseAnimationFadeIn(elem) {
			var item = elem.find('.js-animation-fadeIn');

			TweenLite.to(item, 0.6, { opacity: 0 });
		}

		function animationFadeInMulti(elem, step) {
			var items = elem.find('.js-animation-fadeIn');
			var delayAnimation = step;

			for (var i = 0; i < items.length; i++) {
				TweenLite.to(items[i], 0.6, { opacity: 1, delay: delayAnimation });
				delayAnimation = delayAnimation + step;
			}
		}

		function reverseAnimationFadeInMulti(elem) {
			var items = elem.find('.js-animation-fadeIn');

			for (var i = 0; i < items.length; i++) {
				TweenLite.to(items[i], 0.6, { opacity: 0 });
			}
		}

		function animationSlideLeft(elem) {
			TweenLite.to(elem, 0.6, { left: 0 });
		}

		function reverseAnimationSlideLeft(elem) {
			TweenLite.to(elem, 0.6, { left: -2000 });
		}

		function animationHeightUp(elem, widthElem, heightElem) {
			TweenLite.fromTo(elem, 0.6, { width: 0, height: 0 }, { width: widthElem, height: heightElem });
		}

		function reverseAnimationHeightUp(elem) {
			TweenLite.to(elem, 0.6, { width: 0, height: 0 });
		}


		return {
			animationFadeInUpMulti: animationFadeInUpMulti,
			reverseAnimationFadeInUpMulti: reverseAnimationFadeInUpMulti,
			animationFadeInUp: animationFadeInUp,
			reverseAnimationFadeInUp: reverseAnimationFadeInUp,
			animationFadeInDown: animationFadeInDown,
			reverseAnimationFadeInDown: reverseAnimationFadeInDown,
			animationFadeIn: animationFadeIn,
			reverseAnimationFadeIn: reverseAnimationFadeIn,
			animationFadeInMulti: animationFadeInMulti,
			reverseAnimationFadeInMulti: reverseAnimationFadeInMulti,
			animationSlideLeft: animationSlideLeft,
			reverseAnimationSlideLeft: reverseAnimationSlideLeft,
			animationHeightUp: animationHeightUp,
			reverseAnimationHeightUp: reverseAnimationHeightUp
		};
	}());

	//Функция определения позиции блока при скролле
	function positionBlock(block, selector) {
		var windowBottom = $(window).scrollTop() + $(window).height();
		var block2Bottom = block.offset().top + $(selector).height();
		return windowBottom >= block2Bottom;
	}

	// добавление активного класса пункту меню
	function addActiveMenu(name) {
		var menuItems = $('.Menu__list .Menu__item');

		menuItems.each(function() {
			if(name == $(this).attr('data-name')) {
				$(this).addClass('active');
			} else {
				$(this).removeClass('active');
			}
		});
	}

	// отмена скролла над страницами
	$('.Page__wrapper, .Menu').on ('wheel scroll touchmove mousewheel', function(e) {
		e.stopPropagation();
	});

	// Поэкранная прокрутка
	$('.js-fullpage-wrapper').fullpage({
		navigation: true,
		navigationPosition: 'left',
		lazyLoading: true,
		fadingEffect: true,
		scrollingSpeed: 850,
		menu: '.Menu',
		anchors:['greeting', 'franchise', 'mashines', 'investments', 'pack'],

		//события после загрузки блоков
		afterLoad: function (anchorLink, index) {
			addActiveMenu($(this).attr('data-name'));
			$('.js-menu').removeClass('opened');
			$('.js-overlay').removeClass('active');

			if ($(window).width() > 768) {
				if (index == 1) {
					animationBlock.animationFadeInUpMulti($('.Banner--greeting .js-animation-section-items'), 0.1);
				}

				if (index == 2) {
					animationBlock.animationFadeInUpMulti($('.Banner--franchise-negative .js-animation-section-items'), 0.1);
				}

				if (index == 3) {
					animationBlock.animationFadeInUpMulti($('.Banner--mashines .js-animation-section-items'), 0.1);
				}

				if (index == 4) {
					animationBlock.animationFadeInUpMulti($('.Banner--investments .js-animation-section-items'), 0.1);
				}

				if (index == 5) {
					animationBlock.animationFadeInUpMulti($('.Banner--pack .js-animation-section-items'), 0.1);
					$('.Mouse--wrapper').addClass('hidden');
				}
			}
		},
		onLeave: function (index, nextIndex, direction) {

			if ($(window).width() > 768) {
				if (index == 1) {
					animationBlock.reverseAnimationFadeInUpMulti($('.Banner--greeting .js-animation-section-items'));
				}

				if (index == 2) {
					animationBlock.reverseAnimationFadeInUpMulti($('.Banner--franchise-negative .js-animation-section-items'));
				}

				if (index == 3) {
					animationBlock.reverseAnimationFadeInUpMulti($('.Banner--mashines .js-animation-section-items'));
				}

				if (index == 4) {
					animationBlock.reverseAnimationFadeInUpMulti($('.Banner--investments .js-animation-section-items'));
				}

				if (index == 5) {
					animationBlock.reverseAnimationFadeInUpMulti($('.Banner--pack .js-animation-section-items'));
					$('.Mouse--wrapper').removeClass('hidden');
				}
			}
		}
	});

	var trueItems = [];

	document.querySelectorAll('.side.active').forEach(function (item) {
		var newItem = item.cloneNode(true);
		trueItems.push(newItem);
	});

	function toogleSide() {
		var items = document.querySelectorAll('.side');
		items.forEach(function (item) {
			item.classList.toggle('active');
		});
	}

	function showPEC(button) {
		var filterWord = button.getAttribute('data-category');

		var arrForShow = trueItems.filter(function (item) {
			return item.getAttribute('data-category').indexOf(filterWord) > -1;
		});
		var allBack = document.querySelectorAll('.side:not(.active)');
		document.querySelectorAll('.trueSide').forEach(function (item) {
			console.log(item);
			item.classList.add('empty');
		});
		allBack.forEach(function (item) {
			item.classList.add('empty');
			item.innerHTML = '';
		});

		arrForShow.forEach(function (item, index) {
			document.querySelectorAll('.trueSide')[index].classList.remove('empty');
			allBack[index].classList.remove('empty');
			allBack[index].innerHTML = item.innerHTML;
		});
		toogleSide();
	}

	function filterItems() {
		var container = $('.js-filter-container');
		var navs = container.find('.js-filter-link');

		navs.each(function() {
			var element = $(this);

      element.on('click', function(e) {
				e.preventDefault();

        $(navs).map(function() {
          if ($(this).hasClass('active')) {
            $(this).removeClass('active');
          }
				});

				$(this).addClass('active');

				showPEC(this);
			});
		});
	}

	filterItems();

	// Функция форматирования чисел
	function formatNumber (value) {
		var strNumber = String(value).replace(/(\.(.*))/g, '');
		var arrNumbers = strNumber.split('');
		var numberFormat = '';

		if (strNumber.length > 3) {

			for (var i = arrNumbers.length - 1, j = 1; i >= 0; i--, j++) {
				numberFormat = arrNumbers[i] + numberFormat;
				if (j % 3 == 0) {
					numberFormat = ' ' + numberFormat;
				}
			}

			return numberFormat;
		} else {
			return strNumber;
		}
	}

	// Функция анимирования числа - счетчик
	function counterAnimate(starValue, endValue, selector) {
		var Cont = {val:starValue},
				NewVal = endValue;

		TweenLite.to(Cont, 5, {val: NewVal, roundProps: "val", onUpdate: function () {
				document.querySelector(selector).innerHTML = formatNumber(Cont.val);
			}
		});
	}
});