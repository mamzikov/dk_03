require([
	'jquery',
	'photoswipe',
	'photoswipe-ui',
], function ($, PhotoSwipe, PhotoSwipeUI_Default) {
	var initPhotoSwipeFromDOM = function (gallerySelector) {
		var parseThumbnailElements = function (el) {
			var thumbElements = el.childNodes,
				numNodes = thumbElements.length,
				items = [],
				figureEl,
				linkEl,
				size,
				item;

			for (var i = 0; i < numNodes; i++) {
				figureEl = thumbElements[i];

				if (figureEl.nodeType !== 1) {
					continue;
				}

				linkEl = figureEl.children[0];

				size = linkEl.getAttribute('data-size').split('x');

				item = {
					src: linkEl.getAttribute('href'),
					w: parseInt(size[0], 10),
					h: parseInt(size[1], 10)
				};

				if (figureEl.children.length > 1) {
					item.title = figureEl.children[1].innerHTML;
				}

				if (linkEl.children.length > 0) {
					item.msrc = linkEl.children[0].getAttribute('src');
				}

				item.el = figureEl;
				items.push(item);
			}

			return items;
		};

		// Найти ближайший родительский элемент
		var closest = function closest(el, fn) {
			return el && (fn(el) ? el : closest(el.parentNode, fn));
		};

		var onThumbnailsClick = function (e) {
			e = e || window.event;
			e.preventDefault ? e.preventDefault() : e.returnValue = false;

			var eTarget = e.target || e.srcElement;

			// Найти корневой элемент слайдера
			var clickedListItem = closest(eTarget, function (el) {
				return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
			});

			if (!clickedListItem) {
				return;
			}

			// Найти индекс выбранного элемента путем циклического перехода по всем дочерним элементам
			var clickedGallery = clickedListItem.parentNode,
				childNodes = clickedListItem.parentNode.childNodes,
				numChildNodes = childNodes.length,
				nodeIndex = 0,
				index;

			var childNodes2 = childNodes;

			for (var i = 0; i < numChildNodes; i++) {
				if (childNodes[i].nodeType !== 1) {
					continue;
				}

				if (childNodes[i] === clickedListItem) {
					index = nodeIndex;
					break;
				}
				nodeIndex++;
			}


			if (index >= 0) {
				// Открыть галерею если найден правильный индекс
				openPhotoSwipe(index, clickedGallery);
			}
			return false;
		};

		var openPhotoSwipe = function (index, galleryElement, disableAnimation, fromURL) {
			var pswpElement = document.querySelectorAll('.pswp')[0],
				gallery,
				options,
				items;

			items = parseThumbnailElements(galleryElement);

			// Определяем опции галереи
			options = {
				history: false,
				index: parseInt(index, 10),
				getThumbBoundsFn: function (index) {
					var thumbnail = items[index].el.getElementsByTagName('img')[0],
						pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
						rect = thumbnail.getBoundingClientRect();

					return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
				}
			};

			// Выйти если индекс не найден
			if (isNaN(options.index)) {
				return;
			}

			if (disableAnimation) {
				options.showAnimationDuration = 0;
			}

			// Передача опций и инициализация плагина
			gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
			gallery.init();
		};

		// Перебор элементов галереи
		var galleryElements = document.querySelectorAll(gallerySelector);

		for (var i = 0, l = galleryElements.length; i < l; i++) {
			galleryElements[i].setAttribute('data-pswp-uid', i + 1);
			galleryElements[i].onclick = onThumbnailsClick;
		}
	};

	// Инициализация галереи
	initPhotoSwipeFromDOM('.js-shop-gallery');
	initPhotoSwipeFromDOM('.js-product-image');
	initPhotoSwipeFromDOM('.js-product-gallery');
	initPhotoSwipeFromDOM('.js-ingredient-image');
});