<?php	
if($_POST)
{
	
	///////////////// EDITABLE OPTIONS   /////////////////////

	$receiving_email_address 	= "tip312@yandex.ru"; 
	
	$subject 					= "Контакты с формы сайта франшизы"; 	// the subject line of email.
	
	$name_error_msg 			= "Пожалуйста, введите имя"; 	// error text for name field
	
	$email_error_msg 			= "Please enter a valid email address!"; 		// error text for email field
	
	$msg_error_msg 				= "Please enter message."; 						// error text for message field
	
	$mail_success_msg_heading 	= "Ваши контакты отправлены успешно!";	// error text if mail function failed
	
	$mail_success_msg 			= "Мы свяжемся с вами в ближайшее время.";	// error text if mail function failed
	
	$mail_error_msg 			= "Не удалось отправить контакты! Что то пошло не так...";	// error text if mail function failed
	
	
	// =============================  DO NOT EDIT BELOW THIS LINE  ======================================
	
    
    if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
        
        $output = json_encode(array( 
            'type'=>'error', 
            'text' => 'Sorry Request must be Ajax POST'
        ));
        die($output); 
    } 
    
    //Sanitize input data using PHP filter_var().
    $name      		= filter_var($_POST["name"], FILTER_SANITIZE_STRING);
    $phone      	= filter_var($_POST["phone"], FILTER_SANITIZE_STRING);
    $present      	= (isset($_POST["present"])) ? filter_var($_POST["present"], FILTER_SANITIZE_STRING) : '';
    $email      	= (isset($_POST["email"])) ? filter_var($_POST["email"], FILTER_SANITIZE_EMAIL) : '';
    $locationValue  = (isset($_POST["locationValue"])) ? filter_var($_POST["locationValue"], FILTER_SANITIZE_STRING) : '';
    $time           = (isset($_POST["time"])) ? filter_var($_POST["time"], FILTER_SANITIZE_STRING) : '';
    $domain 		= filter_var($_POST["domain"], FILTER_SANITIZE_URL);
    
    //additional php validation
    if(strlen($name)<4){ 
        $output = json_encode(array('type'=>'error', 'text' => $name_error_msg));
        die($output);
    }
    
    //email body
    $message_body = "Пользователь отправил контактные данные с одной из форм сайта: ".$domain."\r\n\r\nКонтактные данные:\r\n\r\nИмя: ".$name."\r\n\r\nТелефон: ".$phone;
    
    if (!empty($present)) {
        $message_body .= "\r\n\r\nСумма подарка: ".$present;
    }
    if (!empty($time)) {
        $message_body .= "\r\n\r\nУдобное время звонка: ".$time;
    }
    if (!empty($email)) {
        $message_body .= "\r\n\r\nПочта: ".$email;
    }
    if (!empty($locationValue)) {
        $message_body .= "\r\n\r\nРегион: ".$locationValue;
    }
     
    
    //proceed with PHP email.
    $headers = 'From: '.$name. "\r\n" .
    'Reply-To: website@dobryicoffee.ru' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
    
    $send_mail = mail($receiving_email_address, $subject, $message_body, $headers);
    
    if(!$send_mail)
    {
        //If something goes wrong, check your PHP email configuration.
        $output = json_encode(array('type'=>'error', 'text' => $mail_error_msg));
        die($output);
    } else {
        $output = json_encode(array('type'=>'success', 'text' => '<h4>Уважаемый, '.$name.'!</h4><h6>'.$mail_success_msg_heading.'</h6><p>'.$mail_success_msg.'</p>'));
        die($output);
    }
}
?>
